import { Route, Switch, useRouteMatch } from "react-router"
import ProductPage from "../../pages/Product"
import ProductsPage from "../../pages/Products"
import ShoppingPage from "../../pages/Shopping"

const HomeRoutes = () => {
    const { path } = useRouteMatch()

    return (
        <Switch>
            <Route path={`${path}/shopping`} component={ShoppingPage} />
            <Route path={`${path}/products`} component={ProductsPage} />
            <Route path={`${path}/product/:id`} component={ProductPage} />
        </Switch>
    )
}

export default HomeRoutes