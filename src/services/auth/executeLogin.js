import api from '../api'
import { WS_LOGIN } from '../config'

export const executeLogin = async (name, password) => {
    try {
        let response = await api.post(WS_LOGIN, {
            login: name,
            password
        })
        return response.data
    } catch (exception) {
        console.log(exception)
        throw new Error("Não foi possível fazer o login")
    }
}