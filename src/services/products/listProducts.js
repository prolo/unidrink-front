import api from '../api'
import { WS_PRODUCTS } from '../config'

export const listProducts = async (token) => {
    let response = await api.get(WS_PRODUCTS, {
        headers: { Authorization: `Bearer ${token}`}
    })
    return response.data
}