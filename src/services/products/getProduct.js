import api from '../api'
import { WS_PRODUCTS } from '../config'

export const getProduct = async (id, token) => {
    let response = await api.get(`${WS_PRODUCTS}/${id}`, {
        headers: { Authorization: `Bearer ${token}`}
    })
    return response.data
}