import { useState } from 'react';
import './styles.css';
import { Unidrink } from '../../assets';
import { useHistory } from 'react-router'
import { executeLogin } from '../../services/auth'
import { useStore } from '../../store';
import { login } from '../../store/reducers/auth';

function Login() {
  const history = useHistory()
  const [, dispatch] = useStore()

  const [name, setName] = useState('')
  const [password, setPassword] = useState('')

  const handleSubmit = async () => {
    try {
      let result = await executeLogin(name, password)
      dispatch(login(result))
      history.push('/app/products')
    } catch (exception) {
      console.log(exception)
    }
  }

  return (
    <div className='login-container'>
      <div className='card'>
        <img src={Unidrink} className='logo-image' />
        <label>Nome</label>
        <input
            name="name"
            type="text"
            value={name}
            onChange={(e) => { setName(e.target.value) }}
            className='input-field'
        /> <br/><br/>

        <label>Senha</label>
        <input 
            name="password"
            type="password"
            value={password}
            onChange={(e) => { setPassword(e.target.value) }}
            className='input-field'
        /> <br/><br/>

        <button className='login-buttom' onClick={() => { handleSubmit() }}>Login</button>
      </div>
    </div>
  );
}

export default Login;
