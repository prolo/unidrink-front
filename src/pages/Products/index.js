import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from "@mui/material"
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { listProducts } from '../../services/products';
import { useStore } from '../../store';
import './styles.css';

function Products() {
  const [products, setProducts] = useState([])

  const [state] = useStore()
  const { userData } = state
  const history = useHistory()

  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await listProducts(userData.token)
        setProducts(result)
      } catch (exception) {
        if (exception.response.status == 401) {
            history.push('/login')
        }
      }
    }

    fetchData()
  }, [])

  return (
    <div>
      <Typography variant="h5" style={{ marginBottom: "20px", color: 'white' }}>Products Page</Typography>

        <TableContainer component={Paper}>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Produto</TableCell>
                        <TableCell>Marca</TableCell>
                        <TableCell>Categoria</TableCell>
                        <TableCell>Preço</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {
                        products.map(product =>
                            <TableRow key={product.id}>
                                <TableCell><Link to={`/app/product/${product.id}`}>{product.name}</Link></TableCell>
                                <TableCell>{product.brand}</TableCell>
                                <TableCell>{product.productCategory}</TableCell>
                                <TableCell>{Intl.NumberFormat('pt-br', { style: 'currency', currency: 'BRL' }).format(product.sale_price)}</TableCell>
                            </TableRow>
                        )
                    }
                </TableBody>
            </Table>
        </TableContainer>
    </div>
  );
}

export default Products;
