import { Paper, Container, Typography, Button } from '@mui/material';
import { useParams, useHistory } from 'react-router';
import { getProduct } from '../../services/products';
import { useState, useEffect } from 'react';
import { useStore } from '../../store';

import './styles.css';

function Product() {
  const [product, setProduct] = useState({})

  const [state] = useStore()
  const { userData } = state
  const history = useHistory()
  const {id} = useParams()

  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await getProduct(id, userData.token)
        console.log(result)
        setProduct(result)
      } catch (exception) {
        if (exception.response.status == 401) {
            history.push('/login')
        }
      }
    }

    fetchData()
  }, [])

  return (
    <div>
      <Typography variant="h5" style={{ marginBottom: "20px", color: 'white' }}>Product Page</Typography>

      <Container component={Paper} style={{ padding: "20px"}}>
        <Typography variant="h6">Name: {product.name}</Typography>
        <Typography variant="body1">Description: {product.description}</Typography>
        <Typography variant="body2">Brand: {product.brand}</Typography>
        <Typography variant="body2">Category: {product.productCategory}</Typography>
        <Typography variant="body1">Price: {Intl.NumberFormat('pt-br', { style: 'currency', currency: 'BRL' }).format(product.sale_price)}</Typography>
        <Typography variant="body2">Quantity Available: {product.quantity_available}</Typography>
        <Button>Comprar</Button>
      </Container>
    </div>
  );
}

export default Product;
