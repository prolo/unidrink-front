import { AppBar, Button, Toolbar, Typography } from '@mui/material'
import { useHistory, useRouteMatch } from 'react-router'
import { Link } from 'react-router-dom'
import { logout } from '../../store/reducers/auth'
import { useStore } from '../../store'
import { Unidrink } from '../../assets'

const AppToolbar = () => {
    const { url } = useRouteMatch()
    
    const [, dispatch] = useStore()
    const history = useHistory()
    
    const exitApp = () => {
        dispatch(logout())
        history.push('/login')
    }

    const styles = { 
        background: 'white',
        color: 'black',
        fontFamily: 'Ubuntu Mono, monospace'
    }

    return (
        <AppBar position='static' style={styles}>
            <Toolbar>
                <Typography variant="h6">
                    <img src={Unidrink} width="85px"/>
                </Typography>
                <span style={{flexGrow: 1}} />
                <Button color="inherit" component={Link} to={`${url}/products`}>Produtos</Button>
                <Button color="inherit" component={Link} to={`${url}/shopping`}>Carrinho</Button>
                <Button color="inherit" onClick={exitApp}>Sair</Button>
            </Toolbar>
        </AppBar>
    )
}

export default AppToolbar